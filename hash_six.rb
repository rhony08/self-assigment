def letter_count(param)
  params = param.split("")
  output = Hash.new(0)
  for i in 0...params.size do
    output[params[i]] += 1
  end
  output
end

puts "task_six"
puts "---------------------------------------"
puts letter_count("gojek")
puts letter_count("kolla")
puts letter_count("scholarship")