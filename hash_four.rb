def string_from_hash(params)
  output = []
  params.select{ |key, val| output.push("#{key} = #{val}") }
  output.join(',')
end

puts "task_four"
puts "---------------------------------------"
puts string_from_hash({a: 1, b: 2})