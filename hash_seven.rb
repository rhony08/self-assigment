def word_count(param)
  params = param.split(" ").map { |str| str.downcase.gsub(/[^a-z]/, '')}
  output = Hash.new(0)
  for i in 0...params.size do
    output[params[i]] += 1
  end
  output
end

puts "task_seven"
puts "---------------------------------------"
puts word_count("New: Returns a new, empty hash. If this hash is subsequently accessed by a key that does not correspond to a hash entry, the value returned depends on the style of new used to create the hash. In the first form, the access returns nil. If object is specified, this single object will be used for all default values. If a block is specified, it will be called with the hash object and the key, and should return the default value. The block has the responsibility to store the value in the hash if required.")