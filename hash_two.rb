def unique_sum(params)
  uniq_params = params.uniq
  sum = 0
  for i in 0...uniq_params.size do
    sum += uniq_params[i]
  end
  sum
end

puts "task_two"
puts "---------------------------------------"
puts unique_sum([1, 2, 3])
puts unique_sum([1, 3, 8, 1, 8])
puts unique_sum([-1, -1, 5, 2, -7])