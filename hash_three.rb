def find_occurrence(params)
  counter = Hash.new(0)
  for i in 0...params.size do
    counter[params[i]] += 1
  end
  output = counter.select { |key, val| val.odd? }.keys
  output.join(',')
end

puts "task_three"
puts "---------------------------------------"
puts find_occurrence([20,1,-1,2,-2,3,3,5,5,1,2,4,20,4,-1,-2,5])
puts find_occurrence([1,1,2,-2,5,2,4,4,-1,-2,5])
puts find_occurrence([20,1,1,2,2,3,3,5,5,4,20,4,5])
puts find_occurrence([10])
puts find_occurrence([1,1,1,1,1,1,10,1,1,1,1])