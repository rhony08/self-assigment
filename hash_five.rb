def list(params)
  output = ''
  max = params.size-1
  for i in 0..max do
    if i != 0 && i < max
      output += ', '
    elsif i != 0 && i == max
      output += ' & '
    end
    output += params[i][:name]
  end
  output
end

puts "task_five"
puts "---------------------------------------"
puts list([ {name: 'Bart'}, {name: 'Lisa'}, {name: 'Maggie'} ])
puts list([ {name: 'Bart'}, {name: 'Lisa'} ])
puts list([ {name: 'Bart'} ])
puts list([])